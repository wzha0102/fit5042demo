package fit5042.tutex.calculator;


import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import javax.ejb.Stateful;
import fit5042.tutex.repository.entities.Property;

@Stateful
public class ComparePropertySessionBean implements CompareProperty{
	private Set<Property> propertySet = new HashSet<Property>();

	@Override
	public void addProperty(Property property) {
		// TODO Auto-generated method stub
		Optional.ofNullable(property).ifPresent(p -> propertySet.add(p));
		
	}

	@Override
	public void removeProperty(Property property) {
		// TODO Auto-generated method stub
		Iterator<Property> it = propertySet.iterator();
		while (it.hasNext()) {
			if (it.next().getPropertyId() == property.getPropertyId()) {
				it.remove();
				break;
			}
		}
	}

	@Override
	public int getBestPerRoom() {
		// TODO Auto-generated method stub
		if (propertySet.size() < 1)
			return 0;
		List<Property> sortedList = propertySet.stream()
		.sorted(Comparator.comparing(Property::getNumberOfBedrooms, Comparator.reverseOrder()))
		.collect(Collectors.toList());
		return sortedList.get(0).getPropertyId();
	}
	
	
	

}
